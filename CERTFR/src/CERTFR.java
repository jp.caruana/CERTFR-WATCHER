import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;



import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;

import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


import core.printing.Doc;

import core.printing.table.SimpleTable;

public class CERTFR {

	private static  int START = 105;
	private static  int END = 163;
	static ArrayList<String> blacklist = new ArrayList<String>();
	static ArrayList<String> inside_perimer = new ArrayList<String>();
	static ArrayList<String> outside_perimeter = new ArrayList<String>();

	public static void create_report(int start, int end) throws Exception {

		setStartAndEndIndex(start,end);

		

		

		// ,,,Edge,,,,,,,,,,,

		Doc doc;
		FileWriter fw;
		doc = new Doc();

		doc.addSection("T�ches par projet");
		SimpleTable tasktable = doc.addSimpleTable();
		tasktable.add("Objet");
		if (hasPerimeter()){
		  tasktable.add("P�rim�tre");
		}
		tasktable.add("Avis ou Alerte");
		tasktable.add("Risque");
		tasktable.add("Syst�me");
		tasktable.add("R�sum�");
		tasktable.add("Date");
		tasktable.add("Source");
		tasktable.add("Correctif");
		for (int ident = START; ident <= END; ident++) {
			System.out.println("Traitement avis: "
					+ getIdentOnThreeDigit(ident));
			StringBuilder resp = downloadfiles(ident);
			String title = extractVulnerabilityTitle(resp);
			if (!isBlackListed(title)) {

				tasktable.newline();
				tasktable.add(title);
				if (hasPerimeter()){
					
					
					if (isInLot8(title)) {
						tasktable.add("oui");
					} else if (isOutLot8(title)) {
						tasktable.add("non");
	
					} else {
						tasktable.add("");
					}
				}
				tasktable.add("Avis");
				tasktable.add(extractRisk(resp));
				tasktable.add(extractSysteme(resp));

				tasktable.add(extractSummary(resp));
				tasktable.add(extractDate(resp));
				tasktable
						.add("https://www.cert.ssi.gouv.fr/avis/CERTFR-2018-AVI-"
								+ getIdentOnThreeDigit(ident) + "/");
				tasktable.add(extractDocumentation(resp));
			}

		}
		System.out.println("Traitement termin�");

		doc.pop();

		fw = new FileWriter(new File("out/vulnerabilite.html"));
		fw.write(doc.getHTML());
		fw.close();

	}

	private static boolean hasPerimeter() {
		return !(inside_perimer.isEmpty() && outside_perimeter.isEmpty());
	}

	private static String getIdentOnThreeDigit(int ident) {
		if (ident == 2) {
			return "" + ident;
		}
		if (ident < 10) {
			return "00" + ident;
		}
		if (ident < 100) {
			return "0" + ident;
		}
		return ident + "";
	}

	private static boolean isBlackListed(String ident) {
		for (String s : blacklist) {
			if (ident.contains(s)) {
				return true;
			}
		}
		return false;
	}

	private static boolean isInLot8(String ident) {
		for (String s : inside_perimer) {
			if (ident.contains(s)) {
				return true;
			}
		}
		return false;
	}

	private static boolean isOutLot8(String ident) {
		for (String s : outside_perimeter) {
			if (ident.contains(s)) {
				return true;
			}
		}
		return false;
	}

	private static String extractVulnerabilityTitle(StringBuilder resp) {
		String title_balise = "<td class=\"col-xs-4\">Titre</td>";
		int begin = resp.indexOf(title_balise);
		int end = resp.indexOf("</tr>", begin);
		String result = resp.substring(begin + title_balise.length()
				+ 22, end - 6);
		return result;

	}

	private static String extractDate(StringBuilder resp) {
		String title_balise = "<td class=\"col-xs-4\">Date de la première version</td>";
		int begin = resp.indexOf(title_balise);
		int end = resp.indexOf("</tr>", begin);
		String result = resp.substring(begin + title_balise.length()
				+ 22, end - 6);
		return result;

	}

	private static String extractRisk(StringBuilder resp) {
		String title_balise = "<h2>Risque(s)</h2>";
		int begin = resp.indexOf(title_balise);
		int end = resp.indexOf("</ul>", begin);
		String result = resp.substring(
				begin + 1 + title_balise.length(), end);
		return result;

	}

	private static String extractDocumentation(StringBuilder resp) {
		String title_balise = "<h2>Documentation</h2>";
		int begin = resp.indexOf(title_balise);
		int end = resp.indexOf("</ul>", begin);
		String result = resp.substring(
				begin + 1 + title_balise.length(), end + 5);
		return result;

	}

	private static String extractSummary(StringBuilder resp) {
		String title_balise = "<h2>Résumé</h2>";
		
		int begin = resp.indexOf(title_balise);
		int end = resp.indexOf("<h2>", begin+2);
		String summary = resp.substring(
				begin + 4 + title_balise.length(), end);
		return summary;

	}

	private static String extractSysteme(StringBuilder resp) {
		String title_balise = "<h2>Systèmes affectés</h2>";
		int begin = resp.indexOf(title_balise);
		if (begin != -1){ 
			int end = resp.indexOf("<h2>", begin+1);
			String result = resp.substring(
					begin + 1 + title_balise.length(), end);
			return result;
		} else {
			return "non renseign�";
		}

	}

	private static StringBuilder downloadfiles(int ident)
			throws NoSuchAlgorithmException, KeyManagementException,
			KeyStoreException, IOException, ClientProtocolException {
		SSLContext sslcontext = SSLContexts.custom()
				.loadTrustMaterial(null, new TrustStrategy() {

					@Override
					public boolean isTrusted(X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// Trust all certificates..
						return true;
					}
				}).build();

		// Allow TLSv1 protocol only
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
				sslcontext, null, null,
				SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		CloseableHttpClient httpclient = HttpClients.custom()
				.setSSLSocketFactory(sslsf).build();
		HttpGet httpget = new HttpGet(
				"https://www.cert.ssi.gouv.fr/avis/CERTFR-2018-AVI-"
						+ getIdentOnThreeDigit(ident) + "/");
		HttpResponse response = httpclient.execute(httpget);

		InputStream is = response.getEntity().getContent();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		StringBuilder resp = new StringBuilder(); // or StringBuffer if Java
													// version 5+
		String line;
		while ((line = rd.readLine()) != null) {
			resp.append(line);
			resp.append('\r');
		}
		rd.close();
		FileWriter fw = new FileWriter("out/" + "CERTFR-2018-AVI-"
				+ getIdentOnThreeDigit(ident) + ".html");
		fw.write(resp.toString());
		fw.close();
		return resp;

	}

	public static void addBlackListedApp(String app) {
		blacklist.add(app);
		
	}

	public static void setStartAndEndIndex(int i, int j) {
		START = i;
		END = j;
	}

	public static void addInsidePerimeter(String string) {
		inside_perimer.add(string);	
	}

	public static void addOutsidePerimeter(String string) {
		outside_perimeter.add(string);
		
	}

}
